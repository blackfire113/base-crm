#!/bin/bash
yarn
cp .env.example .env
meta git update
meta yarn install

for env in . ./projects/api ./projects/webapp; do
  if [ ! -f ${env}/.env ]; then
    echo "copy ${env}/.env.example"
    cp ${env}/.env.example ${env}/.env
  fi
done

docker-compose up -d
sleep 5
cd ./projects/api
sequelize db:migrate
sequelize db:seed:all
