/* eslint-disable @typescript-eslint/camelcase */
const { PM2_PROJECT } = process.env;
const appsToRun = PM2_PROJECT ? PM2_PROJECT.split(',') : [];
const api = require('./api.pm2');
// const backoffice = require('./backoffice.pm2');

const apps = !appsToRun.length ? [
  api,
  // backoffice
] : [];

if (appsToRun.includes('api')) apps.push(api);
// if (appsToRun.includes('backoffice')) apps.push(backoffice);

module.exports = {
  apps,
};
