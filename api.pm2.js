/* eslint-disable @typescript-eslint/camelcase */

module.exports = {
  name: 'api',
  cwd: './projects/api/',
  script: 'yarn dev',
  args: ['--color', '--respawn', '--clear', '--files'],
  merge_logs: true,
  watch: ['./'],
  watch_options: {
    followSymlinks: true,
    dot: true,
  },
  ignore_watch: ['src/uploads/**/*', '.git/**/*'],
};
